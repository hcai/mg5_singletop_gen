//--------------------LHEtoROOT-----------------------------------------------
//Author: Dan Phan (dan@umail.ucsb.edu)
//Date: December 2014
//Description/Instructions: Go to line 22, change name of file to your LHE file
//Change tree name to your desired root tree name
//Change name of output file to your desired output filename
//----------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include "TTree.h"
#include "TFile.h"
#include "Math/Vector4D.h"
#include <TLorentzVector.h>

using namespace std;

float computePhi(float cosphi, float sinphi) {
  float phi = TMath::ACos(cosphi);
  if(sinphi < 0) return (2*M_PI - phi);
  else return phi;
}

//Parameters
//char* filename  = (char*)"/afs/cern.ch/work/c/cwng/mg5_singletop_gen/run_nonzero_bmass/PROC_dim6top_LO_UFO_0/Events/run_01/unweighted_events.lhe";

char* filename  = (char*)"/eos/user/h/hcai/top/shares/unweighted_events.lhe";
//char* filename = (char*)"/eos/user/h/hcai/top/run_20200617/PROC_dim6top_LO_UFO_0/Events/run_01/unweighted_events.lhe";
char* outputName = (char*)"LHE_Correct_bmass";
char* treeName = (char*)"LHE_Tree";

//fill tree for only maxEvents number of events
int numEvents = 0; //initializing counter of events
int maxEvents = 1e9; //if you want to fill all events, make maxEvents huge

//function to take store reweights
float store_reweights(string line) {

  float wgt;
  int begin = line.find(">"); //beginning of number
  int end = line.find("<",begin);  //end of number
  wgt= ::atof(line.substr(begin+1,end-(begin+1)).c_str()); //defines wgt as substring of line holding reweight
	//between begin and end
  return wgt;
}

int looper(){


  //Declare TTree and TFile
  TFile *file = new TFile(Form("%s.root", outputName), "RECREATE");
  TTree *tree = new TTree("tree", Form("%s",treeName));  //tree called "tree"
  
  //Declare variables that will be stored in tree
  vector <int> pdgID;
  vector <int> status;
  vector <int> mother_1;
  vector <int> mother_2;
  vector <int> colour_1;
  vector <int> colour_2;
  // vector <TLorentzVector> four_momentum;  //encodes 4-momentum and position
	//  vector <TLorentzVector> four_position;
	// kinematic variables
	double top_pt, top_eta, top_phi, top_m;
	double wboson_pt, wboson_eta, wboson_phi, wboson_m;
	double spec_pt, spec_eta, spec_phi;
	double lep_pt, lep_eta, lep_phi, lep_ID;
	double neutrino_pt, neutrino_eta, neutrino_phi, neutrino_pz, neutrino_ID;
	double bottom_pt, bottom_eta, bottom_phi;

	// angular variables
	float cos_theta, cos_theta_star, phi, phi_star;

  int nParticles;      //variables in row1, first row beneath "<event>"
  int process_number;
  float weight;
  float energy_scale;
  float QED_coupling;
  float QCD_coupling;

	//4-vectors of particles in each event for computing angles
	TLorentzVector top, wboson, spec, lep, neutrino, bottom, beampos, beamneg;
	TLorentzVector topT, wbosonT, specT, lepT, bottomT, beamposT, beamnegT;
	TVector3 beta, q, N, T;
	const TVector3 posz(0,0,1), negz(0,0,-1);

  vector <float> reweight; //variable not in main block of code
  
  //Match up variable with branch
  
  //variables to be filled in second_line_below loop
  tree->Branch("pdgID", &pdgID); 
  tree->Branch("status", &status);
  tree->Branch("mother_1", &mother_1);
  tree->Branch("mother_2", &mother_2);
  tree->Branch("colour_1", &colour_1);
  tree->Branch("colour_2", &colour_2);
  //tree->Branch("four_momentum", &four_momentum);
	tree->Branch("top_pt", &top_pt);
	tree->Branch("top_eta", &top_eta);
	tree->Branch("top_phi", &top_phi);
	tree->Branch("top_m", &top_m);
	tree->Branch("wboson_pt", &wboson_pt);
	tree->Branch("wboson_eta", &wboson_eta);
	tree->Branch("wboson_phi", &wboson_phi);
	tree->Branch("wboson_m", &wboson_m);
	tree->Branch("spec_pt", &spec_pt);
	tree->Branch("spec_eta", &spec_eta);
	tree->Branch("spec_phi", &spec_phi);
	tree->Branch("bottom_pt", &bottom_pt);
	tree->Branch("bottom_eta", &bottom_eta);
	tree->Branch("bottom_phi", &bottom_phi);
	tree->Branch("lep_pt", &lep_pt);
	tree->Branch("lep_eta", &lep_eta);
	tree->Branch("lep_phi", &lep_phi);
	tree->Branch("lep_ID", &lep_ID);
	tree->Branch("neutrino_pt", &neutrino_pt);
	tree->Branch("neutrino_eta", &neutrino_eta);
	tree->Branch("neutrino_phi", &neutrino_phi);
	tree->Branch("neutrino_pz", &neutrino_pz);
	tree->Branch("neutrino_ID", &neutrino_ID);
	tree->Branch("cos_theta",&cos_theta);
	tree->Branch("cos_theta_star",&cos_theta_star);
	tree->Branch("phi",&phi);
  tree->Branch("phi_star",&phi_star);
  //tree->Branch("four_position", &four_position);
  
  //filled separate from first_below_event and second_line_below loops
  tree->Branch("reweight",&reweight); 
  
  //filled in first_below_event loop
  tree->Branch("nParticles",&nParticles);
  tree->Branch("process_number",&process_number);
  tree->Branch("weight",&weight);
  tree->Branch("energy_scale",&energy_scale);
  tree->Branch("QED_coupling",&QED_coupling); 
  tree->Branch("QCD_coupling",&QCD_coupling); 
 
  string line; //line of LHE file, to be looped over in main part of looper()

  bool first_line_below = false; //whether on line right below event
  bool second_line_below = false; //between <event> and </event>, is turned to true one line after "first_line_below", both turn off after filling tree
  bool is_reweight = false;  //notes whether data contains reweight data

  //declare vectors used to fill other variables
  vector<float> row;  //row used to fill variables in second_line_below loop
  vector<float> row1; //row used to fill variables in first_line_below part of looper() 

  //Opening data file
  fstream myfile (filename, ios_base::in);

  istringstream iss;
  while (getline(myfile,line)){  //loops through file and fills line(the variable) with that line of the file
    if (numEvents > maxEvents) break;  //fills tree only for maxEvents number of events

    //if on 2nd line below "<event>", fill variables(row1 and reweight entries filled in other loops)
    if (line.find("<event>") != string::npos) {
      std::cout << "Found event\n";
      getline(myfile,line);
      istringstream iss1;
      iss1.str(line);
			TLorentzVector four_momentum_temp;
     
      float val1;
      
      while (iss1 >> val1) row1.push_back(val1);  //fill row1 with numbers in line
      
      nParticles = row1[0];
      process_number = row1[1];
      weight = row1[2];
      energy_scale = row1[3];
      QED_coupling = row1[4];
      QCD_coupling = row1[5];
      std::cout << row1[0] <<"\n";
      row1.clear(); //clears row1 for next event 
      std::cout << nParticles << " Particles found\n";
			int partfound(0); //bit-wise check for essential particles: top(0), wboson(1), spec(2), bottom(3), lepton(4)
			string particleNames[5] = {"Top quark", "W boson", "Spectator quark", "Bottom quark", "Lepton"};
      for (int ipart=0; ipart < nParticles; ipart++) {
				getline(myfile,line);
				//std::cout << "Store particle " << ipart <<"\n";
				istringstream iss;
				iss.str(line);      //copying line into stream
				//std::cout << line << "\n";
				float val; 
				while (iss >> val) row.push_back(val);  //fill row with data from line
				//std::cout << row[0] <<"\n";
	
				//Now split row into vectors for each variable
				pdgID.push_back(row[0]); 
				status.push_back(row[1]);
				mother_1.push_back(row[2]);
				mother_2.push_back(row[3]);
				colour_1.push_back(row[4]);
				colour_2.push_back(row[5]);
				four_momentum_temp.SetPxPyPzE(row[6], row[7], row[8], row[9]); //in GeV
				//four_momentum.push_back(four_momentum_temp);

				//Store four momenta information in corresponding LorentzVectors for angle computation
				if (abs(row[0]) == 6) {
					top = four_momentum_temp;
					top_pt = top.Pt();
					top_eta = top.Eta();
					top_phi = top.Phi();
					top_m = top.M();
					partfound += 1;
				}
				else if (abs(row[0]) == 24) {
					wboson = four_momentum_temp;
					wboson_pt = wboson.Pt();
					wboson_eta = wboson.Eta();
					wboson_phi = wboson.Phi();
					wboson_m = wboson.M();
					partfound += 2;
				}
				else if (abs(row[0]) < 5 && row[1] > 0) {
					spec = four_momentum_temp;
					spec_pt = spec.Pt();
					spec_eta = spec.Eta();
					spec_phi = spec.Phi();
					partfound += 4;
				}
				else if (abs(row[0]) == 5 && row[1] > 0) {
					bottom = four_momentum_temp;
					bottom_pt = bottom.Pt();
					bottom_eta = bottom.Eta();
					bottom_phi = bottom.Phi();
					partfound += 8;
				}
				else if (abs(row[0]) == 11 || abs(row[0]) == 13 || abs(row[0]) == 15) {
					lep = four_momentum_temp;
					lep_pt = lep.Pt();
					lep_eta = lep.Eta();
					lep_phi = lep.Phi();
					lep_ID = row[0];
					partfound += 16;
				}
				else if (abs(row[0]) == 12 || abs(row[0]) == 14 || abs(row[0]) == 16) {
					neutrino = four_momentum_temp;
					neutrino_pt = neutrino.Pt();
					neutrino_eta = neutrino.Eta();
					neutrino_phi = neutrino.Phi();
					neutrino_pz = neutrino.Pz();

					neutrino_ID = row[0];
					partfound += 32;
				}
			
				// LorentzVector four_position_temp;
				// four_position_temp.SetXYZT(row[10],row[11],row[12],0);  //no time information in file, we have set t=0
				// four_position.push_back(four_position_temp);
	
				row.clear();  //clears row so we can fill it again with next line of data
				//std::cout << "Got to end of particles\n";
      }
			//Check if any particle is not registered
			for (int position = 0; position < 6; ++position) {
				if (((partfound >> position) & 1) == 0) cout << particleNames[position] << " is not found!\n";
			}
			
			//Angle computation

			//Boost everything into top rest frame
			beta = -(top.BoostVector());
			topT = top; topT.Boost(beta);
			specT = spec; specT.Boost(beta);
			lepT = lep; lepT.Boost(beta);
			wbosonT = wboson; wbosonT.Boost(beta);
			//beam direction defined as incoming quark direction, of which the mass is assumed to be 0 as we are in ultrarelativistic regime
			beampos.SetVectM(posz,0); beamposT = beampos; beamposT.Boost(beta);
			beamneg.SetVectM(negz,0); beamnegT = beamneg; beamnegT.Boost(beta);
			//qNT axes
			q = specT.Vect();                                           q.SetMag(1); // Z/q axis
			N = ((spec.Eta()>0 ? beamposT : beamnegT).Vect()).Cross(q); N.SetMag(1); // Y/-N axis. N = beam X q
			T = q.Cross(N);                                             T.SetMag(1); // X/T axis = q X N = Z X (-Y) 
			//Theta/Phi angles
			cos_theta = cos(wbosonT.Angle(q));
			phi = computePhi(cos(wbosonT.Angle(T))/sin(wbosonT.Angle(q)), cos(wbosonT.Angle(-N))/sin(wbosonT.Angle(q)));

			//Boost into W rest frame
			beta = -(wboson.BoostVector());
			bottomT = bottom; bottomT.Boost(beta);
			lepT = lep; lepT.Boost(beta);
			specT = spec; specT.Boost(beta);
			//qNT axes in W rest frame
			q = -bottomT.Vect(); q.SetMag(1);       // Z/q axis = original direction of W boson = opposite direction of bottom quark
			N = specT.Vect().Cross(q); N.SetMag(1); // Y/N axis = spectator quark X q
			T = N.Cross(q); T.SetMag(1);            // X/T axis = N X q = Y \cross Z
			//Theta*/Phi* angles
			cos_theta_star = cos(lepT.Angle(q));
			phi_star = computePhi(cos(lepT.Angle(T))/sin(lepT.Angle(q)), cos(lepT.Angle(N))/sin(lepT.Angle(q)));

    }
    //Store reweights 
    if (line.find("wgt id") != string::npos && line.find("opt") != string::npos) { //if line contains wgt id, start filling reweight

      //wgt is substring of line that contains the reweight information
      float wgt = store_reweights(line);
      reweight.push_back(wgt); //fill reweight vector

      is_reweight = true; //note that events contain reweight information
    }

    //Check every line after </event> to see if we've reached the next event 
    if ((line.find("</event>") != string::npos)) {  //if line contains  "</event>", we turn first_line_below to true(begins filling row1 variables)
      std::cout<< "Found end of event "<< numEvents <<" \n";
      //if file doesn't contain reweight info, fill reweight with nonsense 
      if (is_reweight == false) reweight.push_back(-5);

      if (pdgID.size() != 0){
        tree->Fill();   //only fill tree if vectors are nonempty(pdgID picked arbitrarily)
        numEvents++;

      }
  
      //clear vectors so we can fill them again for next event
      pdgID.clear();  
      status.clear();
      mother_1.clear();
      mother_2.clear();
      colour_1.clear();
      colour_2.clear();
      reweight.clear();
      //four_momentum.clear();
      //four_position.clear();

      first_line_below = true; //begins process to move onto next event
    }
    
  }

  //  tree->Fill(); //last event isn't filled, because there's no next "<event>" to trigger it

  file->cd();
  tree->Write(); 

  return 0;
}
