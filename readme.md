- Setup

please make sure using SLC6 machines (lxplus6), not fully developed for lxplus7 due to the CentOS7 status.

```
setupATLAS -c centos7
asetup AthGeneration,21.6.55
```

- run local job
	- Change the run directory name and the configurations as you like
```
mkdir run; cd run
Gen_tf.py --ecmEnergy=13000. --maxEvents=10000 --firstEvent=1 --randomSeed=123456 --outputEVNTFile=output.root --jobConfig=../100001/
```

- run on the grid
```
tar -czvf mg5gen.tgz 100001/
pathena --trf "Gen_tf.py --ecmEnergy=13000. --maxEvents=-1 --firstEvent=1 --randomSeed=%RNDM:1000 --jobConfig=100001/ --outputEVNTFile=%OUT.root" --outDS=user.${USER}.`date +'%Y%m%d'`.singleTop_fourAngle.mg5pythia8.evtGen.raw.root --nEventsPerJob=100 --split=1000 --inTarBall=mg5gen.tgz
```

- Other info:

JIRA ticket: https://its.cern.ch/jira/browse/ATLMCPROD-8747
Spread sheet for each mc campaign:
mc16a: https://docs.google.com/spreadsheets/d/1Qe5yrJktmC_fXNN9OJKd685OsSxZal7gD7z-ztlPigA/edit#gid=0
mc16d: https://docs.google.com/spreadsheets/d/1E_JAZYKH4qlc0-igw6bji2Sx5QPhtMgiIbXYAbSsz84/edit#gid=0
mc16e: https://docs.google.com/spreadsheets/d/1P-HT87mcAzZ6fL76xEDUujD2ZJZhXcuXcIlI_0VXJsA/edit#gid=0
