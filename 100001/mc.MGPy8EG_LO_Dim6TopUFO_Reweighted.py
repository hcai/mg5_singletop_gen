import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os

from shutil import copyfile

nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
nevents *= 1.1 # safety factor


process="""
    import model dim6top_LO_UFO
    define l+ = e+ mu+ ta+
    define vl = ve vm vt
    define l- = e- mu- ta-
    define vl~ = ve~ vm~ vt~
    define jq = u d s c u~ d~ s~ c~
    generate p p > t jq b~ $$ w+ QED=2 DIM6=1 FCNC=0, (t > w+ b QED=2 DIM6=1 FCNC=0, w+ > l+ vl)
    add process p p > t~ jq b $$ w- QED=2 DIM6=1 FCNC=0, (t~ > w- b~ QED=2 DIM6=1 FCNC=0, w- > l- vl~)
    output -f"""


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = {  'lhe_version':'2.0',
            'cut_decays':'F',
            'ptj': '0.0',
            'bwcutoff':'50',
            'nevents':int(nevents),
}

params ={}
masses = {'6' : 172.5}
sminputs = {'aEWM1' : 1.323489e+02}
params['MASS']=masses
params['SMINPUTS']=sminputs
process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
modify_param_card(process_dir=process_dir,params=params)

# generate reweight parameters                                                                                                                                                                                                                                                                                            
ctW_param = []
IctW_param = []
cptb_param = []
cbW_param = []
IcbW_param = []
cqQ_param = []
opt_name = []

from math import sqrt

xw=80.399/172.5
slope=round(xw/2/sqrt(2),2)

base_ctW = 1.0
base_IctW = 0.5
base_cptb = 5
base_cbW = 1.0
base_IcbW = 1.0
base_cqQ = 1.5


optNameFormat="ctW{:.1f}_IctW{:.1f}_cptb{:.1f}_cbW{:.1f}_IcbW{:.1f}_cqQ{:.1f}"
count=0
for i_ctW in [-3,-2,0,2,3]:
    for i_IctW in [-3,-2,0,2,3]:
        for i_cptb in [-3,-2,0,2,3]:
            for i_cbW in [-3,-2,0,2,3]:
                for i_IcbW in [-3,-2,0,2,3]:
                    for i_cqQ in [-2,0,2]:
                        test=i_ctW**2+i_IctW**2+i_cptb**2+i_cbW**2+i_IcbW**2+i_cqQ**2
                        if 0.5<test<16.5:
                            ctW_param.append(i_ctW*base_ctW)
                            IctW_param.append(i_IctW*base_IctW)
                            cptb_param.append(i_cptb*base_cptb)
                            cbW_param.append(i_cbW*base_cbW + slope*cptb_param[-1])
                            IcbW_param.append(i_IcbW*base_IcbW)
                            cqQ_param.append(i_cqQ*base_cqQ)
                            #print(count,test,optNameFormat.format(ctW_param,IctW_param,cptb_param,cbW_param,IcbW_param,cqQ_param))
                            opt_name.append(optNameFormat.format(ctW_param[-1],IctW_param[-1],cptb_param[-1],cbW_param[-1],IcbW_param[-1],cqQ_param[-1]))
                            count +=1

reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("change helicity False\n")
for option in range(count):
    reweight_card_f.write("launch --rwgt_info={}\n".format(opt_name[option]))
    reweight_card_f.write("       set DIM6 8 {:+.1f}\n".format(cptb_param[option]))
    reweight_card_f.write("       set DIM6 10 {:+.1f}\n".format(ctW_param[option]))
    reweight_card_f.write("       set DIM6 12 {:+.1f}\n".format(IctW_param[option]))
    reweight_card_f.write("       set DIM6 14 {:+.1f}\n".format(cbW_param[option]))
    reweight_card_f.write("       set DIM6 15 {:+.1f}\n".format(IcbW_param[option]))
    reweight_card_f.write("       set DIM6 58 {:+.1f}\n".format(cqQ_param[option]))    
reweight_card_f.close()

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


############################
# Shower JOs will go here
evgenConfig.nEventsPerJob = 10000
evgenConfig.description = 'MadGraph_singleTop'                                                                                                                 
evgenConfig.contact = ['huacheng.cai@cern.ch', 'petr.jacka@cern.ch']
evgenConfig.keywords+=['singleTop']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
